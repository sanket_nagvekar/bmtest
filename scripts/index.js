$(document).ready(function(){
// Sticky Nevbar
  $(window).scroll(function() {
      if ($(window).scrollTop() > 100) {
          $('.main-header').addClass('sticky');
      } else {
          $('.main-header').removeClass('sticky');
      }
  });

  // Mobile Navigation
  $('.mobile-toggle').click(function() {
      if ($('.main-header').hasClass('open-nav')) {
          $('.main-header').removeClass('open-nav');
      } else {
          $('.main-header').addClass('open-nav');
      }
  });

  $('.main-header li a').click(function() {
      if ($('.main-header').hasClass('open-nav')) {
          $('.main-header').removeClass('open-nav');
      }
  });


//Slider hero
setInterval(function () { moveRight();}, 4000);
 var slideCount = $('#slider ul.slide-wrapper li').length;
 var slideWidth = $('#slider ul.slide-wrapper li').width();
 var slideHeight = $('#slider ul.slide-wrapper li').height();
 $('.hero-slider').css({ height: slideHeight });
 $('#slider ul.slide-wrapper li:last-child').prependTo('#slider ul.slide-wrapper');
function moveRight() {
     $('#slider ul.slide-wrapper').animate({transform: - slideHeight}, 300, function() {
       $('#slider ul.slide-wrapper li').removeClass('active');
       $('#slider ul.slide-wrapper li:first-child').addClass('active');
         $('#slider ul.slide-wrapper li:first-child').appendTo('#slider ul.slide-wrapper');
         $('#slider ul.slide-wrapper').css('transform', '');
        var index = $('#slider ul.slide-wrapper li:first-child').data('index');
       paginationUpdate(index);
     });
 };
 $( window ).resize(function() {
   var slideCount = $('#slider ul.slide-wrapper li').length;
   var slideWidth = $('#slider ul.slide-wrapper li').width();
   var slideHeight = $('#slider ul.slide-wrapper li').height();
    $('.hero-slider').css({ height: slideHeight });
 });
function paginationUpdate(i){
 $('#slider ul.pagi li').removeClass('active');
 $('#slider ul.pagi li').eq(i-1).addClass('active');
};

$('nav a').click(function(event) {
  event.preventDefault();
    var id = $(this).attr("href");
    var offset = 90;
    var target = $(id).offset().top - offset;
    $('html, body').animate({
        scrollTop: target
    }, 500);
});

});
